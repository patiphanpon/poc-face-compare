const fs = require('fs')

const { defineConfig } = require('@vue/cli-service')

// module.exports = defineConfig({
//   transpileDependencies: true,
// })

module.exports = {
  devServer: {
    port: 8080,
    https: {
      key: fs.readFileSync('./certs/dev.local+5-key.pem'),
      cert: fs.readFileSync('./certs/dev.local+5.pem'),
    },
  }
}
